###################
Installing firmware
###################

If your hardware can boot of an SD-card meta-ts will generate a
`WIC <https://www.yoctoproject.org/docs/2.4.2/dev-manual/dev-manual.html#creating-partitioned-images-using-wic>`_
image which you can ``dd`` to your target.  Otherwise the firmware must be
flashed in a board specific way.

Since the firmware provides a [UEFI]_ interface you are free to choose the
distro you prefer.

QEMU arm64
**********

QEMU just needs the build file containing all the firmware binaries.

.. note::

   Files needed from build directory **flash.bin**

SynQuacer
*********

The SynQuacer can't boot from an SD card.  You need to download and install the
firmware via ``xmodem``.  You can find detailed instructions
`here <https://www.96boards.org/documentation/enterprise/developerbox/installation/board-recovery.md.html#update-using-serial-flasher>`_

The short version is flip DSW2-7 to enable the serial flasher, open your
minicom and use ``xmodem`` to send and update the files.

.. code-block:: bash

    flash write cm3 ->  Control-A S   (send scp_romramfw_Release.bin)
    flash rawwrite 0x600000 0x400000  (Control-A S -> fip.bin-synquacer)

After successful firmware update via serial flasher, power off the board,
set DSW2-7 to OFF, DSW3-3 and DSW3-4 to ON to enable OP-TEE and
TBB(Trusted Board Boot).

.. note::

   Files needed from build directory **scp_romramfw_release.bin**, **fip.bin**

rockpi4b
********

.. code-block:: bash

   zcat ts-firmware-rockpi4b.rootfs.wic.gz > /dev/sdX

.. note::

   Files needed from build directory **ts-firmware-rockpi4b.rootfs.wic.gz**

Raspberry Pi4
*************

.. code-block:: bash

   zcat ts-firmware-rpi4.wic.gz > /dev/sdX

.. note::

   Files needed from build directory **ts-firmware-rpi4.wic.gz**

Xilinx KV260 AI Starter kit
***************************

This board uses an internal SPI flash.  You need to reset the board while
pressing ``FWUEN`` switch.  This will launch an HTTP server at ``192.168.0.111``

Connect to the web Interface and update ImageA and ImageB

.. note::

   Files needed from build directory **ImageA.bin**, **ImageB.bin**

