##########
References
##########

.. [UEFI] `Unified Extensable Firmware Interface Specification v2.9
   <https://uefi.org/sites/default/files/resources/UEFI_Spec_2_9_2021_03_18.pdf>`_,
   February 2020, `UEFI Forum <http://www.uefi.org>`_

.. [EBBR] `Embedded Base Boot Requirements v2.0.0-pre1
   <https://arm-software.github.io/ebbr/>`_,
   January 2021, `Arm Limited <http://arm.com>`_

.. [fTPM] `Firmware TPM
   <https://www.microsoft.com/en-us/research/publication/ftpm-software-implementation-tpm-chip/>`_,
   August 2016, `Microsoft <http://www.microsoft.com>`_

.. [SWTPM] `Software TPM
   <https://github.com/stefanberger/swtpm>`_

.. [EFI_TCG_Protocol] `EFI TCG protocol
   <https://trustedcomputinggroup.org/resource/tcg-efi-protocol-specification>`_

.. [TCG_PC_Client_Spec] `TCG PC Client Specific Platform Firmware Profile Specification
   <https://trustedcomputinggroup.org/resource/pc-client-specific-platform-firmware-profile-specification>`_

.. [MBFW] `Multi-bank firmware updates
   <https://gitlab.com/Linaro/trustedsubstrate/mbfw/uploads/3d0d7d11ca9874dc9115616b418aa330/mbfw.pdf>`_

.. [FWU] `Platform Security Firmware Update for the A-profile Arm Architecture 1.0
   <https://developer.arm.com/documentation/den0118/a>`_,
   May 2021, `Arm Limited <http://arm.com>`_
