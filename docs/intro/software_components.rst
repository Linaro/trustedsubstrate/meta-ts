############################
Software components overview
############################
A variety of firmware components is used to achieve SystemReady compliance with
the security features needed for modern connected devices.

Generally speaking boards use a combination of the following software components
to boot up and setup their chain of trust

- `U-Boot <https://source.denx.de/u-boot/u-boot>`_
- `OP-TEE <https://github.com/OP-TEE>`_
- `TF-A <https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git/>`_
- `firmware TPM <https://github.com/microsoft/ms-tpm-20-ref>`_
- `StandAloneMM from EDK2 <https://github.com/tianocore/edk2-platforms.git>`_
- `SCP <https://github.com/ARM-software/SCP-firmware>`_

A high level overview of the boot chain looks like this.

.. uml::

    object BL2 {
        U-Boot SPL
	    or
        TF-A BL2
    }
    object BL31 {
        Secure Monitor
    }
    object BL32 {
        OP-TEE
	    fTPM
	    StandAloneMM
    }
    object BL33 {
	    U-Boot
    }
    object OS {
        OS with UEFI
    }

    BL2 --> BL31
    BL2 --> BL32
    BL2 --> BL33
    BL33--> OS : UEFI Secure and Measured Boot

