DESCRIPTION = "Helper script to flash i.MX 8M Plus Verdin device with uuu"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"
DEPENDS = "upx-native"

SRC_URI = " \
    https://artifacts.toradex.com/artifactory/tezi-oe-prod-frankfurt/dunfell-5.x.y/release/19/verdin-imx8mp/tezi/tezi-run/oedeploy/Verdin-iMX8MP_ToradexEasyInstaller_5.7.5+build.19.zip;name=tezi \
    https://github.com/nxp-imx/mfgtools/releases/download/uuu_1.5.165/uuu;name=uuu \
"
SRC_URI[tezi.sha256sum] = "2d5c4b56f3ec8d63c14ed9d10620651bbb790153e05451e9e915ce817a2f45e7"
SRC_URI[uuu.sha256sum] = "f863bba022202361d19e5026be0af408d307f78d2dbf2c139fb7eaaabd220442"

S = "${WORKDIR}/sources"
UNPACKDIR = "${S}"

inherit deploy

do_configure[noexec] = "1"
do_compile[noexec] = "1"
do_install[noexec] = "1"

addtask deploy before do_build after do_compile

do_deploy() {
    imx_boot=$(find Verdin-iMX8MP_ToradexEasyInstaller* -type f -name imx-boot)
    install -D -p -m 0644 ${imx_boot} ${DEPLOYDIR}/imx-boot
    install -D -p -m 0755 uuu ${DEPLOYDIR}/uuu
    upx --best --lzma ${DEPLOYDIR}/uuu
}
