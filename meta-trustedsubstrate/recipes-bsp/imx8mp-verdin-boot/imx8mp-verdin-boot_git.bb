DESCRIPTION = "Generate bootloader for i.MX 8M Plus Verdin device"
LICENSE = "GPL-2.0-only"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/GPL-2.0-only;md5=801f80980d171dd6425610833a22dbe6"
DEPENDS = "dtc-native optee-os u-boot"
SRCREV = "cbb99377cc2bb8f7cf213794c030e1c60423ef1f"
SRCBRANCH = "lf-6.6.3_1.0.0"
PV = "0.1+${SRCBRANCH}"

SRC_URI = "git://github.com/nxp-imx/imx-mkimage.git;protocol=https;branch=${SRCBRANCH} \
    file://0001-tee-imx8mp-allow-to-override-the-BL32-base-address.patch \
"

S = "${WORKDIR}/git"

inherit deploy

do_configure[noexec] = "1"
do_install[noexec] = "1"

CFLAGS = "-O2 -Wall -std=c99 -I ${STAGING_INCDIR_NATIVE} -L ${STAGING_LIBDIR_NATIVE}"

MKIMAGE_DTBS ?= "imx8mp-verdin-wifi-dev.dtb"
MKIMAGE_SUPP_DTBS ?= ""
MKIMAGE_SOC ?= "iMX8MP"
MKIMAGE_SOC_PATH ?= "${S}/iMX8M"
MKIMAGE_TARGET ?= "flash_spl_uboot"

do_compile[depends] += "optee-os:do_install u-boot:do_deploy"
do_compile() {
    cp ${COMPONENTS_DIR}/${MACHINE_ARCH}/optee-os/sysroot-only/firmware/tee.bin \
       ${COMPONENTS_DIR}/${MACHINE_ARCH}/u-boot/sysroot-only/firmware/* \
    ${MKIMAGE_SOC_PATH}
    mv ${MKIMAGE_SOC_PATH}/mkimage ${MKIMAGE_SOC_PATH}/mkimage_uboot
    (cd ${S} && TEE_LOAD_ADDR=${TEE_LOAD_ADDR} oe_runmake SOC=${MKIMAGE_SOC} \
        dtbs=${MKIMAGE_DTBS} supp_dtbs=${MKIMAGE_SUPP_DTBS} ${MKIMAGE_TARGET})
}

addtask deploy before do_build after do_compile

do_deploy() {
    install -D -p -m 0644 ${MKIMAGE_SOC_PATH}/flash.bin ${DEPLOYDIR}/flash.bin

    bbplain "[INFO] Generate uuu.auto"
    cat > ${DEPLOYDIR}/uuu.auto << EOF
uuu_version 1.5.165

SDPS: boot -f imx-boot

SDPV: jump

CFG: FB: -vid 0x0525 -pid 0x4000
CFG: FB: -vid 0x0525 -pid 0x403a
CFG: FB: -vid 0x0525 -pid 0x403d
CFG: FB: -vid 0x0525 -pid 0x403f
CFG: FB: -vid 0x0525 -pid 0x4040
CFG: FB: -vid 0x0525 -pid 0x4041
CFG: FB: -vid 0x0525 -pid 0x4042
CFG: FB: -vid 0x0525 -pid 0x4046

FB: ucmd setenv fastboot_dev mmc
FB: ucmd setenv mmcdev 2
FB: ucmd mmc dev 2

FB: flash bootloader flash.bin

FB: ucmd if env exists emmc_ack; then ; else setenv emmc_ack 0; fi;

FB: done
EOF
}
