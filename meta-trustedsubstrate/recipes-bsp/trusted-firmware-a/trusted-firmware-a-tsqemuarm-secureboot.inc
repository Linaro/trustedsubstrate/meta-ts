# qemuarm64-secureboot  machines specific TFA support

COMPATIBLE_MACHINE = "tsqemuarm-secureboot"

TFA_PLATFORM = "qemu"
# TFA_SPD = "opteed"
TFA_UBOOT = "1"
TFA_BUILD_TARGET = "clean all fip"
TFA_INSTALL_TARGET = "flash.bin"

DEPENDS:append= " optee-os u-boot"

EXTRA_OEMAKE += " \
    ARCH=aarch32 ARM_ARCH_MAJOR=7 \
    AARCH32_SP=optee ARM_TSP_RAM_LOCATION=tdram BL32_RAM_LOCATION=tdram \
    BL32=${STAGING_DIR_TARGET}${nonarch_base_libdir}/firmware/tee-header_v2.bin \
    BL32_EXTRA1=${STAGING_DIR_TARGET}${nonarch_base_libdir}/firmware/tee-pager_v2.bin \
    BL32_EXTRA2=${STAGING_DIR_TARGET}${nonarch_base_libdir}/firmware/tee-pageable_v2.bin \
    TRUSTED_BOARD_BOOT=1 \
    GENERATE_COT=1 \
    CREATE_KEYS=1 \
    DECRYPTION_SUPPORT=aes_gcm \
    ENCRYPT_BL31=1 \
    ENCRYPT_BL32=1 \
    ENABLE_STACK_PROTECTOR=strong \
    KEY_ALG=rsa \
    KEY_SIZE=4096 \
    MEASURED_BOOT=1 \
    EVENT_LOG_LEVEL=10 \
    KEY_ALG=rsa \
    MBOOT_EL_HASH_ALG=sha256 \
    "

# Enable Pointer Authentication
EXTRA_OEMAKE:append = "${@bb.utils.contains('MACHINE_FEATURES', 'pointer-auth', \
                                            'BRANCH_PROTECTION=1 CTX_INCLUDE_PAUTH_REGS=1 ARM_ARCH_MINOR=5 ', '',d)}"

do_compile:append() {
    # Create a secure flash image for booting AArch64 Qemu. See:
    # https://git.trustedfirmware.org/TF-A/trusted-firmware-a.git/tree/docs/plat/qemu.rst
    dd if=/dev/zero of=${BUILD_DIR}/flash.bin bs=1M count=64
    dd if=${BUILD_DIR}/bl1.bin of=${BUILD_DIR}/flash.bin bs=4096 conv=notrunc
    dd if=${BUILD_DIR}/fip.bin of=${BUILD_DIR}/flash.bin seek=64 bs=4096 conv=notrunc
}
