SUMMARY = "Firmware config for RPi4"
LICENSE = "MIT"

SRC_URI = "\
    file://LICENSE \
    file://config.txt \
    file://tpm-soft-spi.dts \
"

LIC_FILES_CHKSUM = "file://LICENSE;md5=e874ee1fc7e7471d0747b632bb172ce3"

inherit deploy
DEPENDS = "dtc-native rpi-bootfiles"

S = "${UNPACKDIR}"

FILES:${PN} = "firmware"
do_install() {
    install -d ${D}/firmware/overlays
    install -D ${UNPACKDIR}/config.txt ${D}/firmware/
    dtc -O dtb -b 0 -@ ${UNPACKDIR}/tpm-soft-spi.dts -o ${D}/firmware/overlays/tpm-soft-spi.dtbo
}

# Only include dtb
do_deploy() {
    # Copy the images to deploy directory
    cp -rf ${D}/firmware/* ${DEPLOYDIR}/
}
addtask deploy after do_install
