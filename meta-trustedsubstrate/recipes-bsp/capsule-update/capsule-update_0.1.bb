LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

REQUIRED_MACHINE_FEATURES = "capsule-update"

inherit deploy python3native features_check

S = "${UNPACKDIR}"

SRC_URI = "\
    file://${MACHINE}-images.json \
    file://${UEFI_CAPSULE_CERT_FILE};unpack=false \
    file://${UEFI_CAPSULE_INVALID_CERT_FILE};unpack=false \
"

DEPENDS = "\
	coreutils-native \
	firmware-packager-native \
	gcab-native \
	ovmf-native \
	python3-pyopenssl-native \
	trusted-firmware-a \
	u-boot \
"

# If certs not deployed
ALLOW_EMPTY:${PN} = "1"

do_compile() {
	mkdir -p uefi_capsule_certs
	tar xpvfz "${UNPACKDIR}/${UEFI_CAPSULE_CERT_FILE}" -C uefi_capsule_certs
    mkdir -p uefi_capsule_invalid_certs
    tar xpvfz "${UNPACKDIR}/${UEFI_CAPSULE_INVALID_CERT_FILE}" -C uefi_capsule_invalid_certs

	cp uefi_capsule_certs/CRT.pem CRT.pem
	cp uefi_capsule_certs/CRT.pub.pem CRT.pub.pem

	if [ -e ${DEPLOY_DIR_IMAGE}/fip_all_arm_tf_optee.bin ]; then cp ${DEPLOY_DIR_IMAGE}/fip_all_arm_tf_optee.bin . ; fi
	if [ -e ${DEPLOY_DIR_IMAGE}/fip.bin ]; then cp ${DEPLOY_DIR_IMAGE}/fip.bin . ; fi
	if [ -e ${DEPLOY_DIR_IMAGE}/u-boot.bin ]; then cp ${DEPLOY_DIR_IMAGE}/u-boot.bin . ; fi
	if [ -e ${DEPLOY_DIR_IMAGE}/optee/tee-pager_v2.bin ]; then cp ${DEPLOY_DIR_IMAGE}/optee/tee-pager_v2.bin . ; fi
	if [ -e ${DEPLOY_DIR_IMAGE}/idbloader.img ]; then cp ${DEPLOY_DIR_IMAGE}/idbloader.img . ; fi
	if [ -e ${DEPLOY_DIR_IMAGE}/u-boot.itb ]; then cp ${DEPLOY_DIR_IMAGE}/u-boot.itb . ; fi
	if [ -e ${DEPLOY_DIR_IMAGE}/ImageA.bin ]; then cp ${DEPLOY_DIR_IMAGE}/ImageA.bin . ; fi
	if [ -e ${DEPLOY_DIR_IMAGE}/ImageB.bin ]; then cp ${DEPLOY_DIR_IMAGE}/ImageB.bin . ; fi

	export PYTHONPATH="${STAGING_DIR_NATIVE}/usr/bin/edk2_basetools/BaseTools/Source/Python:${PYTHONPATH}"
	python3 ${STAGING_DIR_NATIVE}/usr/bin/edk2_basetools/BaseTools/Source/Python/Capsule/GenerateCapsule.py -j ${MACHINE}-images.json \
		-e -o ${MACHINE}_fw.capsule \
		--verbose

	GUID=`fgrep -m 1 Guid ${MACHINE}-images.json | awk -F '"' '{print $4}'`
	${STAGING_DIR_NATIVE}/${bindir}/firmware_packager/firmware_packager.py --firmware-name \
		${MACHINE} --release-version 0.2 \
		--bin `pwd`/${MACHINE}_fw.capsule \
		--out ${MACHINE}_fw.cab \
		--device-guid ${GUID} \
		--developer-name Linaro \
		--version-format number \
		--update-protocol org.uefi.capsule

    # Generate capsule with an invalid signature
    cp uefi_capsule_invalid_certs/CRT.pem CRT.pem
    cp uefi_capsule_invalid_certs/CRT.pub.pem CRT.pub.pem

    python3 ${STAGING_DIR_NATIVE}/usr/bin/edk2_basetools/BaseTools/Source/Python/Capsule/GenerateCapsule.py -j ${MACHINE}-images.json \
        -e -o ${MACHINE}_fw_invalid_sig.capsule \
        --verbose

}

do_deploy() {
	mkdir -p ${DEPLOYDIR}

	cp ${MACHINE}_fw.cab ${DEPLOYDIR}/
	cp ${MACHINE}_fw.capsule ${DEPLOYDIR}/
    cp ${MACHINE}_fw_invalid_sig.capsule ${DEPLOYDIR}/
}

addtask deploy before do_build after do_compile

CAPSULE_DEPENDS = " trusted-firmware-a:do_deploy u-boot:do_deploy"
do_compile[depends] .= "${CAPSULE_DEPENDS}"
