# Generate Rockchip style loader binaries

SRC_URI:append:rpi4 = " file://${MACHINE}.cfg"
COMPATIBLE_MACHINE:rpi4 = "rpi4"

DEPENDS += "rpi4-config"

UBOOT_BOARDDIR = "${S}/board/raspberrypi/rpi"
UBOOT_ENV_NAME = "rpi.env"
