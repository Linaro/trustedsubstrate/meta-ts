# Generate Android boot images for Qualcomm boards.

# We need 2025.04 rcX for now, in order not to carry intrernal patches.
# We can update to -master after 2025.04
PV = "2025.04-rc3"
SRCREV = "0fd7ee0306a88bd99d3820167fa45a7ee6fbc1e1"
COMPATIBLE_MACHINE = "qcom-rbx"

SRC_URI += "file://${MACHINE}.cfg"
SRC_URI:remove = "file://v3_20250120_sughosh_ganu_add_pmem_node_for_preserving_distro_iso_s.patch"
SRC_URI += "file://v6_20250228_sughosh_ganu_add_pmem_node_for_preserving_distro_iso_s.patch"
SRC_URI += "file://0001-dt-reinstall.patch"
SRC_URI += "file://gpt_both0.bin"

# We pack U-Boot into an Android boot image.
DEPENDS += "android-tools-native"
DEPENDS += "xxd-native"

UBOOT_BOARDDIR = "${S}/board/qualcomm"

QCOM_RBX_DEVICES = "rb1:qrb2210-rb1 rb2:qrb4210-rb2 rb3:sdm845-db845c rb5:qrb5165-rb5"

# Qualcomm boards chainload U-Boot by packing it into an Android boot image.
# Do that here, generating a boot image for each board.
do_compile:append() {
	# Upstream mkbootimg is dumb and breaks if you don't give it a ramdisk.
	touch ${B}/empty-ramdisk
	cat ${B}/qcom_defconfig/u-boot-nodtb.bin | gzip - > ${B}/u-boot-nodtb.bin.gz
	for device in ${QCOM_RBX_DEVICES}; do
		BOARD=${device%:*}
		DTB=${device#*:}

		# Generate the boot image
		cat ${B}/u-boot-nodtb.bin.gz > ${B}/u-boot-${BOARD}.bin.gz
		cat ${B}/qcom_defconfig/dts/upstream/src/arm64/qcom/${DTB}.dtb \
			>> ${B}/u-boot-${BOARD}.bin.gz
		mkbootimg --base 0x0 --kernel_offset 0x8000 \
			--ramdisk_offset 0x01000000 \
			--tags_offset 0x100 \
			--pagesize 4096 \
			--ramdisk ${B}/empty-ramdisk \
			--kernel ${B}/u-boot-${BOARD}.bin.gz \
			--output ${B}/u-boot-${BOARD}.img
	done
}

do_configure:append() {
	if "${@bb.utils.contains('MACHINE_FEATURES', 'auth-capsule-update', 'true', 'false', d)}"; then
		test -f "${KCONFIG_CONFIG_ROOTDIR}/.config"
		echo CONFIG_EFI_CAPSULE_ESL_FILE=\"uefi_capsule_certs/CRT.esl\" >> "${KCONFIG_CONFIG_ROOTDIR}/.config"
		grep CONFIG_EFI_CAPSULE_ESL_FILE "${KCONFIG_CONFIG_ROOTDIR}/.config"
	fi
}

do_deploy:append() {
	for device in ${QCOM_RBX_DEVICES}; do
		BOARD=${device%:*}
		install -m 0644 ${B}/u-boot-${BOARD}.img ${DEPLOYDIR}/u-boot-${BOARD}.img
		install -m 0644 ${UNPACKDIR}/gpt_both0.bin ${DEPLOYDIR}/
	done
}
