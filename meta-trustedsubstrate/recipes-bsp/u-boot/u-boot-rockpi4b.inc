# Generate Rockchip style loader binaries
inherit deploy

FILESEXTRAPATHS:prepend := "${THISDIR}/u-boot/${MACHINE}:"

SRC_URI += "file://${MACHINE}.cfg"
SRC_URI += "file://0001-u-boot-rockpi4-add-optee-and-reserved-memory-nodes.patch"
SRC_URI += "file://0002-rockpi4b-fix-capsules-update.patch"
SRC_URI += "file://0001-rockpi4b-set-baud-rate-to-115200.patch"
SRC_URI += "file://signature.dts"

SRC_URI:remove = "file://v3_20240418_ilias_apalodimas_enable_setvariable_RT.patch"
SRC_URI:remove = "file://v2_20240425_ilias_apalodimas_efi_loader_enable_queryvariableinfo_at_runtime_for_file_backed_variable.patch"
SRC_URI:remove = "file://20240403_heinrich_schuchardt_efi_loader_fixes_for_efi_variables.patch"

COMPATIBLE_MACHINE = "rockpi4b"

DEPENDS += "trusted-firmware-a gnutls-native"

UBOOT_BOARDDIR = "${S}/board/rockchip/evb_rk3399"
UBOOT_ENV_NAME = "evb_rk3399.env"

do_compile:prepend() {
	export BL31="${RECIPE_SYSROOT}/firmware/bl31.elf"
	ls -l ${BL31}
	export TEE="${RECIPE_SYSROOT}/lib/firmware/tee.bin"
	ls -l ${TEE}

	for config in ${UBOOT_MACHINE}; do
		mkdir -p uefi_capsule_certs
		tar xpvfz "${UNPACKDIR}/${UEFI_CAPSULE_CERT_FILE}" -C uefi_capsule_certs

		cp uefi_capsule_certs/CRT.pem ${B}/${config}/CRT.pem
		cp uefi_capsule_certs/CRT.pub.pem ${B}/${config}/CRT.pub.pem

		cp ${UNPACKDIR}/signature.dts ${B}/${config}/signature.dts
		export CAPSULE_SIG=${B}/${config}/signature.dts
		ESL="`pwd`/uefi_capsule_certs/CRT.esl"
		sed -i "s|CRT.esl|${ESL}|" ${CAPSULE_SIG}
	done
}

do_deploy:append() {
	mkdir -p ${DEPLOYDIR}
	cp "${KCONFIG_CONFIG_ROOTDIR}/idbloader.img"  "${DEPLOYDIR}/idbloader.img"
	cp "${KCONFIG_CONFIG_ROOTDIR}/u-boot.itb" "${DEPLOYDIR}/u-boot.itb"
}

ATF_DEPENDS = " trusted-firmware-a:do_deploy"
do_compile[depends] .= "${ATF_DEPENDS}"
