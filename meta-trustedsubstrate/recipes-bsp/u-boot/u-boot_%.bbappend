FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:${THISDIR}/u-boot/common:${THISDIR}/${PN}/${MACHINE}:"

# Always increment PR on u-boot config change or patches
PR .= "ts3"

# for tools/mkeficapsule
DEPENDS += "${@bb.utils.contains('MACHINE_FEATURES', 'capsule-update', 'gnutls-native', '', d)}"

# Required for cert-to-efi-sig-list tool
DEPENDS += "efitools-native"

SRC_URI += "file://trs.env"
SRC_URI += "file://0001-Allow-pass-cert-to-efi-sig-list-from-environment.patch"
SRC_URI += "file://v3_20250120_sughosh_ganu_add_pmem_node_for_preserving_distro_iso_s.patch"
SRCREV:remove = "file://0001-scripts-dtc-pylibfdt-libfdt.i_shipped-Use-SWIG_Appen.patch"

SRC_URI += "file://${UEFI_CAPSULE_CERT_FILE};unpack=false"

SRC_URI += "file://common_opts.cfg"
SRC_URI += "${@bb.utils.contains('MACHINE_FEATURES', 'uefi-http', 'file://http_boot.cfg', '', d)}"
SRC_URI += "${@bb.utils.contains('MACHINE_FEATURES', 'uefi-https', 'file://https_boot.cfg', '', d)}"
SRC_URI += "${@bb.utils.contains('MACHINE_FEATURES', 'capsule-update', 'file://capsule_update.cfg', '', d)}"
SRC_URI += "${@bb.utils.contains('MACHINE_FEATURES', 'auth-capsule-update', 'file://auth_capsule_update.cfg', '', d)}"
SRC_URI += "${@bb.utils.contains('MACHINE_FEATURES', 'optee', 'file://optee.cfg', '', d)}"
SRC_URI += "${@bb.utils.contains("MACHINE_FEATURES", "optee-ftpm", "file://optee-ftpm.cfg", "", d)}"
SRC_URI += "${@bb.utils.contains("MACHINE_FEATURES", "optee-stmm", "file://efi_vars_rpmb.cfg", "file://efi_vars_file.cfg", d)}"
SRC_URI += "${@bb.utils.contains("MACHINE_FEATURES", "tpm2", "file://tpm2.cfg", "", d)}"
SRC_URI += "${@bb.utils.contains("MACHINE_FEATURES", "ts-smm-gateway", "file://ffa.cfg", "", d)}"

include u-boot-${MACHINE}.inc

do_configure:prepend() {
	cp -r ${UNPACKDIR}/*_defconfig ${S}/configs/ || true

	# Hack for TRS CI. Each build is done in separated Bitbake build, so
	# previously keys cannot be reused.
	# A better solution would moving this logic to the root Makefile in TRS
	# repo, but TRS CI doesn't use that Makefile which it makes more difficult
	# to reproduce build failures
	rm -rf ${TOPDIR}/sbkeys
	mkdir -p ${TOPDIR}/sbkeys
	tar xvfz ${UEFI_CERT_FILE} -C ${TOPDIR}/sbkeys
}

do_configure:append() {
	if "${@bb.utils.contains('MACHINE_FEATURES', 'auth-capsule-update', 'true', 'false', d)}"; then
		mkdir -p ${B}/../git/uefi_capsule_certs
		tar xpvfz "${UNPACKDIR}/${UEFI_CAPSULE_CERT_FILE}" -C ${B}/../git/uefi_capsule_certs
		# make sure it exists, in case paths or something changes
		test -f "${KCONFIG_CONFIG_ROOTDIR}/.config"
		echo CONFIG_EFI_CAPSULE_CRT_FILE=\"uefi_capsule_certs/CRT.crt\" >> "${KCONFIG_CONFIG_ROOTDIR}/.config"
		grep CONFIG_EFI_CAPSULE_CRT_FILE "${KCONFIG_CONFIG_ROOTDIR}/.config"
	fi

	if [ "${@bb.utils.contains('MACHINE_FEATURES', 'disable-console', '1', '0', d)}" = "1" ] ; then
		echo "CONFIG_BOOTMENU_DISABLE_UBOOT_CONSOLE=y" >> "${KCONFIG_CONFIG_ROOTDIR}/.config"
		echo "CONFIG_AUTOBOOT_MENU_SHOW=y" >> "${KCONFIG_CONFIG_ROOTDIR}/.config"
	fi

	if [ "${@bb.utils.contains('MACHINE_FEATURES', 'silence-console', '1', '0', d)}" = "0" ] ; then
		sed -i '/silent=1/d' ${UNPACKDIR}/trs.env
	fi

	cp ${UNPACKDIR}/trs.env ${UBOOT_BOARDDIR}/${UBOOT_ENV_NAME}
}

export CERT_TO_EFI_SIG_LIST="${STAGING_BINDIR_NATIVE}/cert-to-efi-sig-list"
