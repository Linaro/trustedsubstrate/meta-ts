# Machine specific configurations

MACHINE_OPTEE_OS_REQUIRE ?= ""
MACHINE_OPTEE_OS_REQUIRE:synquacer = "optee-os-tadevkit-synquacer.inc"
MACHINE_OPTEE_OS_REQUIRE:rockpi4b = "optee-os-tadevkit-rockpi4b.inc"

require ${MACHINE_OPTEE_OS_REQUIRE}

# default now in meta-arm, not ok for meta-ts firmware
EXTRA_OEMAKE:remove = "CFG_MAP_EXT_DT_SECURE=y"

# copy to build dir to avoid embedding build time apsolute paths into
# CFG_STMM_PATH which goes into binaries and exported headers and scripts
EXTRA_OEMAKE += "${@bb.utils.contains('MACHINE_FEATURES', 'optee-stmm', \
    'CFG_STMM_PATH=../build/uefi.bin', '', d)}"

do_compile:prepend() {
    if ${@bb.utils.contains('MACHINE_FEATURES','optee-stmm','true','false',d)}; then
        cp ${DEPLOY_DIR_IMAGE}/uefi.bin ${B}
    fi
}
