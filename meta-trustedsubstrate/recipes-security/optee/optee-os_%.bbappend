DEPENDS += "dtc-native"

MACHINE_OPTEE_OS_REQUIRE ?= ""
MACHINE_OPTEE_OS_REQUIRE:rockpi4b = "optee-os-rockpi4b.inc"
MACHINE_OPTEE_OS_REQUIRE:synquacer = "optee-os-synquacer.inc"
MACHINE_OPTEE_OS_REQUIRE:tsqemuarm-secureboot = "optee-os-qemuarm.inc"
MACHINE_OPTEE_OS_REQUIRE:zynqmp-kria-starter = "optee-os-zynmp.inc"
MACHINE_OPTEE_OS_REQUIRE:zynqmp-kria-starter-psa = "optee-os-zynmp.inc"
MACHINE_OPTEE_OS_REQUIRE:imx8mp-verdin = "optee-os-imx8mp-verdin.inc"

require ${MACHINE_OPTEE_OS_REQUIRE}

# Add PKCS11 as early TA
DEPENDS += "python3-cryptography-native"
EXTRA_OEMAKE += "CFG_IN_TREE_EARLY_TAS=pkcs11/fd02c9da-306c-48c7-a49c-bbd827ae86ee"
EXTRA_OEMAKE += "CFG_CORE_HEAP_SIZE=131072"
EXTRA_OEMAKE += "CFG_CORE_BGET_BESTFIT=y"
# OP-TEE log level 2 is INFO
EXTRA_OEMAKE += "CFG_TEE_CORE_LOG_LEVEL=2"
EXTRA_OEMAKE += "CFG_RPMB_FS_CACHE_ENTRIES=48"

# default now in meta-arm, not ok for meta-ts firmware
EXTRA_OEMAKE:remove = "CFG_MAP_EXT_DT_SECURE=y"

# copy to build dir to avoid embedding build time paths into CFG_STMM_PATH
# which lands in binaries and exported headers and scripts
EXTRA_OEMAKE += "${@bb.utils.contains('MACHINE_FEATURES', 'optee-stmm', \
    'CFG_STMM_PATH=../build/uefi.bin', '', d)}"

do_compile:prepend() {
    if ${@bb.utils.contains('MACHINE_FEATURES','optee-stmm','true','false',d)}; then
        cp ${DEPLOY_DIR_IMAGE}/uefi.bin ${B}
    fi
}
