COMPATIBLE_MACHINE:zynqmp-kria-starter-psa = "zynqmp-kria-starter-psa"

FILESEXTRAPATHS:prepend:zynqmp-kria-starter-psa := "${THISDIR}/files/zynqmp-kria-starter-psa:"

SRC_URI:append:zynqmp-kria-starter-psa  = " \
    file://0004-smm_gateway-GetNextVariableName-Fix.patch     \
    file://0011-Fix-Avoid-redefinition-of-variables.patch \
    file://0012-Fix-GetNextVariableName-NameSize-input.patch \
    file://0013-Fix-error-handling-of-variable-index-loading.patch \
    "
