COMPATIBLE_MACHINE:rpi4 = "${MACHINE}"

DEPENDS:remove = "rpi-cmdline"
do_deploy[depends] = "rpi-config:do_deploy"

# from meta-raspberrypi machine configs
BOOTFILES_DIR_NAME ?= "bootfiles"

do_install:append() {
    install -d "${D}/firmware/overlays"
    install -D "${S}/bcm2711-rpi-4-b.dtb" "${D}/firmware/"
    install -D ${S}/start4.elf ${D}/firmware/
    install -D ${S}/fixup4.dat ${D}/firmware/
    install -D ${S}/overlays/* ${D}/firmware/overlays
}

do_deploy:append() {
    # Copy the images to deploy directory
    cp -rf ${D}/firmware/* ${DEPLOYDIR}/
}
