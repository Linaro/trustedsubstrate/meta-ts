Introduction
------------
This repository contains the Linaro Trustedsubstrate layers for OpenEmbedded.

Building
--------

Make sure kas is installed on your system:

```kas build kas/<platform.yml>```

or

```kas-container build kas/<platform.yml>```, if your OS is not supported by OE/Yocto.

Supported platforms:
- qemuarm64-secureboot
- rockpi4b
- zynqmp-kria-starter
- zynqmp-zcu102

For more information on using the OpenEmbedded layer look at
https://trs.readthedocs.io/en/latest/firmware/index.html

CI
--

Latest daily build:
<a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
<img src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge" />
</a>

<!--
  Gitlab allows certain HTML tags to be added as Markdown.
  The full list of allowed tags is here: https://github.com/gjtorikian/html-pipeline/blob/a363d620eba0076479faad86f0cf85a56b2b3c0a/lib/html/pipeline/sanitization_filter.rb
  Also note that there cannot be spaces between tags, otherwise HTML parser doesn't work
-->

<table style="width:100%">
  <thead>
    <th></th>
    <th>QEMU<hr>qemuarm64-secureboot</th>
    <th>QEMU EDK2<hr>(N/A)</th>
    <th>Rockpi4b<hr>rockpi4b</th>
    <th>Xilinx kv260<hr>zynqmp-kria-starter</th>
    <th>Xilinx zcu102<hr>zynqmp-zcu102</th>
  <thead>
  <tbody>
    <!-- build suite -->
    <tr>
      <th>build</th>
      <!-- qemu -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- qemu-edk2 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu-edk2&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rockpi4b -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rk3399-rock-pi-4b&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- kv260 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=kv260&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- zcu102 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=zcu102&suite=build&passrate&title&hide_zeros=1" />
        </a>
      </td>
    </tr>
    <!-- end of build suite -->
    <!-- boot suite (wait-is-system-running) -->
    <tr>
      <th>boot</th>
      <!-- qemu -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu&suite=wait-is-system-running&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- qemu-edk2 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=qemu-edk2&suite=wait-is-system-running&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- rk3399-rock-pi-4b -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=rk3399-rock-pi-4b&suite=wait-is-system-running&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- kv260 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=kv260&suite=wait-is-system-running&passrate&title&hide_zeros=1" />
        </a>
      </td>
      <!-- zcu102 -->
      <td>
        <a href="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/tests">
          <img  src="https://qa-reports.linaro.org/blueprints/nightly/build/latest-finished/badge?environment=zcu102&suite=wait-is-system-running&passrate&title&hide_zeros=1" />
        </a>
      </td>
    </tr>
    <!-- end of boot suite -->
  </tbody>
</table>


Images
------
  * Download images:
    * qemu
        * [flash.bin-qemu.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/flash.bin-qemu.gz?job=build-meta-ts-qemuarm64-secureboot)
    * rockpi4b
        * [ts-firmware-rockpi4b.rootfs.wic.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ts-firmware-rockpi4b.rootfs.wic.gz?job=build-meta-ts-rockpi4b)
    * kv260
        * [ImageA.bin.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ImageA.bin.gz?job=build-kas-meta-ts-zynqmp-starter)
        * [ImageB.bin.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ImageB.bin.gz?job=build-kas-meta-ts-zynqmp-starter)
    * zcu102
        * [ts-firmware-zynqmp-zcu102.rootfs.wic.gz](https://gitlab.com/Linaro/blueprints/nightly-builds/-/jobs/artifacts/main/raw/images/ts-firmware-zynqmp-zcu102.rootfs.wic.gz?job=build-kas-meta-ts-zynqmp-starter)
